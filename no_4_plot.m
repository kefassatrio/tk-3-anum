function no_4_plot()
  x = linspace(1960,2020,2020-1960);
  y = no_4_newtons_divided_difference(x);
  plot(x,y);
end
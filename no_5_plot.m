function no_5_plot()
  x = linspace(1960,2020,2020-1960);
  y = no_5_cubic_spline_for_plot(x);
  plot(x,y);
end
function result = enam_romberg()
  h = (2020 - 1960) / 6;
  i1 = enam_composite_trapezoidal(h);
  i2 = enam_composite_trapezoidal(h / 2);
  i4 = enam_composite_trapezoidal(h / 8);
  i3 = enam_composite_trapezoidal(h / 4);
  
  i1_prime = i2 + ((1 / 3) * (i2 - i1));
  i2_prime = i3 + ((1 / 3) * (i3 - i1));
  i3_prime = i4 + ((1 / 3) * (i4 - i1));
  
  i1_doub_prime = i2_prime + ((1 / 3) * (i2_prime - i1_prime));
  i2_doub_prime = i3_prime + ((1 / 3) * (i3_prime - i2_prime));
  
  result = i2_doub_prime + ((1 / 3) * (i2_doub_prime - i1_doub_prime));
  
function result = base_B(t,i)
  result = (t-1970)**i;
end
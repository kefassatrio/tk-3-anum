function simp_result = enam_composite_simpson(n, a, b)
  h = (b - a) / n;
  x = [a : h : b];
  fa = pol6_best(a);
  fb = pol6_best(b);
  mult_by_4 = 0;
  mult_by_2 = 0;
  m4_limit = n / 2;
  m2_limit = (n / 2) - 1;

  for i = 1:m4_limit
      curr_x = x(2*i);
      pol = pol6_best(curr_x);
      mult_by_4 = mult_by_4 + (4 * pol);
  endfor
  
  for i = 1:m2_limit
      curr_x = x((2*i) + 1);
      pol = pol6_best(curr_x);
      mult_by_2 = mult_by_2 + (2 * pol);
  endfor
  simp_result = (h / 3) * (fa + mult_by_2 + mult_by_4 + fb);
  
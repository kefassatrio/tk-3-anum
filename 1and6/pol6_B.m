function result = pol6_B()
  y = [97.02;119.21;147.49;179.38;206.26;237.63;270.20];
  
  A = [base_B(1960,0) base_B(1960,1) base_B(1960,2) base_B(1960,3) base_B(1960,4) base_B(1960,5) base_B(1960,6);
  base_B(1970,0) base_B(1970,1) base_B(1970,2) base_B(1970,3) base_B(1970,4) base_B(1970,5) base_B(1970,6);
  base_B(1980,0) base_B(1980,1) base_B(1980,2) base_B(1980,3) base_B(1980,4) base_B(1980,5) base_B(1980,6);
  base_B(1990,0) base_B(1990,1) base_B(1990,2) base_B(1990,3) base_B(1990,4) base_B(1990,5) base_B(1990,6);
  base_B(2000,0) base_B(2000,1) base_B(2000,2) base_B(2000,3) base_B(2000,4) base_B(2000,5) base_B(2000,6);
  base_B(2010,0) base_B(2010,1) base_B(2010,2) base_B(2010,3) base_B(2010,4) base_B(2010,5) base_B(2010,6);
  base_B(2020,0) base_B(2020,1) base_B(2020,2) base_B(2020,3) base_B(2020,4) base_B(2020,5) base_B(2020,6);];
  
  result = A \ y;
  
  one = cond(A,1);
  two = cond(A,2);
  three = cond(A,3);
  infinity = cond(A,inf);
  
  printf("Condition Number with Norm-1 is %d \n", one)
  printf("Condition Number with Norm-2 is %d \n", two)
  printf("Condition Number with Norm-3 is %d \n", three)
  printf("Condition Number with Norm-infinity is %d \n", infinity)
end
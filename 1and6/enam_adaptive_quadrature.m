function adapt_result = enam_adaptive_quadrature()
  tol = 10000;
  ranges = [];
  
  ranges(1, :) = [1960, 2020];
  count = 0;
  row = 1;
  while row <= length(ranges(:, 1))
    curr_range = ranges(row, :);
    a = curr_range(1);
    b = curr_range(2);
    m = (a + b) / 2;
    i1 = enam_composite_simpson(2, a, b);
    i2 = enam_composite_simpson(4, a, b);
    
    if ((1 / 15) * abs(i1 - i2)) < tol
      count = count + i2;
    else
      ranges(row + 1, :) = [a m];
      ranges(row + 2, :) = [m b];
    endif
    row = row + 1;
  endwhile
  
  adapt_result = count;

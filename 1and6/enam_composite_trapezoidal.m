function trap_result = enam_composite_trapezoidal(h)
  x = [1960 : h : 2020];
  fa = pol6_best(1960);
  fb = pol6_best(2020);
  mult_by_2 = 0;
  m2_limit = length(x) - 1;
  
  for i = 2:m2_limit
      curr_x = x(i);
      pol = pol6_best(curr_x);
      mult_by_2 = mult_by_2 + (2 * pol);
  endfor
  
  trap_result = (h / 2) * (fa + mult_by_2 + fb);
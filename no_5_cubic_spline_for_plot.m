function result = no_5_cubic_spline_for_plot(x0)
 n = 6;
 x = zeros(1,n+1);
 a = zeros(1,n+1);
 x(1) = 1960;
 x(2) = 1970;
 x(3) = 1980;
 x(4) = 1990;
 x(5) = 2000;
 x(6) = 2010;
 x(7) = 2020;
 
 a(1) = 97.02;
 a(2) = 119.21;
 a(3) = 147.49;
 a(4) = 179.38;
 a(5) = 206.26;
 a(6) = 237.63;
 a(7) = 270.20;

 m = n - 1;
 h = zeros(1,m+1);
 for i = 0:m
   h(i+1) = x(i+2) - x(i+1);
 end

 xa = zeros(1,m+1);
 for i = 1:m
   xa(i+1) = 3.0.*(a(i+2).*h(i)-a(i+1).*(x(i+2)-x(i))+a(i).*h(i+1))./(h(i+1).*h(i));
 end

 xl = zeros(1,n+1);
 xu = zeros(1,n+1);
 xz = zeros(1,n+1);
 xl(1) = 1;
 xu(1) = 0;
 xz(1) = 0;

 for i = 1:m
   xl(i+1) = 2.*(x(i+2)-x(i))-h(i).*xu(i);
   xu(i+1) = h(i+1)./xl(i+1);
   xz(i+1) = (xa(i+1)-h(i).*xz(i))./xl(i+1);
 end

 xl(n+1) = 1;
 xz(n+1) = 0;
 b = zeros(1,n+1);
 c = zeros(1,n+1);
 d = zeros(1,n+1);
 c(n+1) = xz(n+1);

 for i = 0:m
   j = m-i;
   c(j+1) = xz(j+1)-xu(j+1).*c(j+2);
   b(j+1) = (a(j+2)-a(j+1))./h(j+1) - h(j+1) .* (c(j+2) + 2.0 .* c(j+1)) ./ 3.0;
   d(j+1) = (c(j+2) - c(j+1)) ./ (3.0 .* h(j+1));
 end
 
 result = 0;
 lower_bounds = zeros(1,columns(x0));
 indexes = zeros(1,columns(x0));

lower_bound = 1960;
bool = (x0 >= lower_bound & x0 <= lower_bound+10);
for i = 1:columns(bool)
  if (bool(i) == 1)
    indexes(i) = 1;
    lower_bounds(i) = lower_bound;
  endif
endfor

lower_bound = 1970;
bool = (x0 >= lower_bound & x0 <= lower_bound+10);
for i = 1:columns(bool)
  if (bool(i) == 1)
    indexes(i) = 2;
    lower_bounds(i) = lower_bound;
  endif
endfor

lower_bound = 1980;
bool = (x0 >= lower_bound & x0 <= lower_bound+10);
for i = 1:columns(bool)
  if (bool(i) == 1)
    indexes(i) = 3;
    lower_bounds(i) = lower_bound;
  endif
endfor

lower_bound = 1990;
bool = (x0 >= lower_bound & x0 <= lower_bound+10);
for i = 1:columns(bool)
  if (bool(i) == 1)
    indexes(i) = 4;
    lower_bounds(i) = lower_bound;
  endif
endfor

lower_bound = 2000;
bool = (x0 >= lower_bound & x0 <= lower_bound+10);
for i = 1:columns(bool)
  if (bool(i) == 1)
    indexes(i) = 5;
    lower_bounds(i) = lower_bound;
  endif
endfor

lower_bound = 2010;
bool = (x0 >= lower_bound & x0 <= lower_bound+10);
for i = 1:columns(bool)
  if (bool(i) == 1)
    indexes(i) = 6;
    lower_bounds(i) = lower_bound;
  endif
endfor

result += a(indexes)+b(indexes).*(x0-lower_bounds)+c(indexes).*(x0-lower_bounds).^2+d(indexes).*(x0-lower_bounds).^3;

end
 % Newton's divided difference
 % credits to: https://www.youtube.com/watch?v=NCxqXBv5FfY
 
 function result = newtons_divided_difference(n)
    
 x = zeros(1,n+1);
 y = zeros(n+1,n+1);
 
 for i = 0:n
   fprintf('Masukan x(%d) and f(x(%d)) di barisan terpisah:  \n', i, i);
   x(i+1) = input(' ');
   y(i+1,1) = input(' ');
 end
 fprintf('Masukan point yang akan mengevaluasi polynomial nya\n')
 x0 = input('x = ');
 
 n = size(x,1);
 if n == 1
    n = size(x,2);
 end

 for i = 1:n
    D(i,1) = y(i);
 end

 for i = 2:n
    for j = 2:i
       D(i,j)=(D(i,j-1)-D(i-1,j-1))/(x(i)-x(i-j+1));
    end
 end

 fx0 = D(n,n);
 for i = n-1:-1:1
    fx0 = fx0*(x0-x(i)) + D(i,i);
 end
result = strcat('Hasil:',num2str(fx0));

end
function result = horner(x)
  poly = [237630, 110906, 39718, -109389, -302653, -230172, -55860];
  result = poly(1);
  
  for i = 2:length(poly)
    result = (result * x) + poly(i);
  endfor
  
  result

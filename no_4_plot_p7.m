function no_4_plot_p7()
  x = linspace(1960,2020,2020-1960);
  y = no_4_newtons_divided_difference_p7(x);
  plot(x,y);
end
 % Newton's divided difference
 % credits to: https://www.youtube.com/watch?v=NCxqXBv5FfY
 function result = no_4_newtons_divided_difference(x0)
    
 x = zeros(1,7);
 y = zeros(7,7);
 x(1) = 1960;
 x(2) = 1970;
 x(3) = 1980;
 x(4) = 1990;
 x(5) = 2000;
 x(6) = 2010;
 x(7) = 2020;
 
 y(1,1) = 97.02;
 y(2,1) = 119.21;
 y(3,1) = 147.49;
 y(4,1) = 179.38;
 y(5,1) = 206.26;
 y(6,1) = 237.63;
 y(7,1) = 270.20;
 
 n = size(x,1);
 if n == 1
    n = size(x,2);
 end

 for i = 1:n
    D(i,1) = y(i);
 end

 for i = 2:n
    for j = 2:i
       D(i,j)=(D(i,j-1)-D(i-1,j-1))./(x(i)-x(i-j+1));
    end
 end

 fx0 = D(n,n);
 for i = n-1:-1:1
    fx0 = fx0.*(x0-x(i)) + D(i,i);
 end
result = fx0;

end